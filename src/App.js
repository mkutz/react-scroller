import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class Scroller extends Component {
	constructor(p){
		super(p);
		this.state = {
			left: 0
		};
		this.onWheel = this.onWheel.bind(this);
	}
	onWheel(e) {
		const newLeft = this.state.left - e.deltaY * 5;
		this.setState({
			left: newLeft
		})
		e.preventDefault();
	}
	render() {
		return (
			<div 
				style={{width: window.innerWidth, display:'flex',height:2000,position: 'relative'}}
				onWheel={this.onWheel}
			>
				<div 
					style={{
						left: this.state.left,
						position: 'absolute'
					}}
				>
					{this.props.children}
				</div>
			</div>
		)
	}
}

class App extends Component {
  render() {
    return (
      <div className="App">
		<Scroller>
			<div 
				style={{
					width: 2000,
					display: 'inline-block',
				}}>
				<img src="googl.png" />
				<img src="googl.png" />
				<img src="googl.png" />
				<img src="googl.png" />
				<img src="googl.png" />
			</div>
		</Scroller>
      </div>
    );
  }
}

export default App;
